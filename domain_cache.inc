<?php

/**
 * Returns the cache prefix based on the current domain.
 *
 * If domain access is enabled it will use the domain id, otherwise 0.
 */
function domain_cache_get_prefix() {
  global $_domain;
  static $prefix;
  if (empty($prefix)) {
    $prefix = 0;
    if (isset($_domain['domain_id'])) {
      $prefix = $_domain['domain_id'];
    }
  }
  return $prefix;
}

/**
 * Returns a list of available domain ids.
 */
function domain_cache_get_domain_ids() {
  static $domain_ids;
  if (empty($domain_ids)) {
    try {
      $query = db_select('domain', 'd')
        ->fields('d', ['domain_id']);
      $result = $query->execute();
      $domain_ids = $result->fetchCol();
    }
    catch (PDOException $exception) {
      // The domain module might not be installed, just return and empty array.
      $domain_ids = [];
    }
  }
  return $domain_ids;
}

/**
 * Returns a list of excluded bins.
 */
function domain_cache_get_excluded_bins() {
  $bins = variable_get('domain_cache_excluded_bins', array());
  return array_filter($bins);
}
