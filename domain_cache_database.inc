<?php

require_once dirname(__FILE__) . '/domain_cache.inc';

/**
 * Defines a domain based database cache implementation.
 */
class DrupalDomainDatabaseCache extends DrupalDatabaseCache {

  protected $excluded = FALSE;

  /**
   * Constructs a new DrupalDomainDatabaseCache object.
   */
  function __construct($bin) {
    if (in_array($bin, domain_cache_get_excluded_bins())) {
      $this->excluded = TRUE;
    }
    parent::__construct($bin);
  }

  /**
   * Implements DrupalCacheInterface::get().
   */
  function get($cid) {
    // We don't prefix the cache ID here because this just calls getMultiple()
    // which handles the prefixing.
    return parent::get($cid);
  }

  /**
   * Implements DrupalCacheInterface::getMultiple().
   */
  function getMultiple(&$cids) {
    if (!$this->excluded) {
      if ($prefix = domain_cache_get_prefix()) {
        foreach ($cids as $key => $cid) {
          $cids[$key] = $prefix . ':' . $cid;
        }
      }
    }
    return parent::getMultiple($cids);
  }

  /**
   * Implements DrupalCacheInterface::set().
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    if (!$this->excluded) {
      if ($prefix = domain_cache_get_prefix()) {
        $cid = $prefix . ':' . $cid;
      }
    }
    parent::set($cid, $data, $expire);
  }

  /**
   * Implements DrupalCacheInterface::clear().
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    if (function_exists('domain_cache_get_domain_ids')) {
      if (!$this->excluded && $cid != '*') {
        if (!empty($cid)) {
          foreach (domain_cache_get_domain_ids() as $domain_id) {
            if (is_array($cid)) {
              foreach ($cid as &$id) {
                $id = $domain_id . ':' . $id;
              }
              parent::clear($cid, $wildcard);
            }
            else {
              parent::clear($domain_id . ':' . $cid, $wildcard);
            }
          }
        }
      }
    }
    parent::clear($cid, $wildcard);
  }

}
