## INSTALLATION ##

Step 1. 
--------------------------------------------------------------------------------
Add the following code to settings.php that corresponds to your current cache 
backend.

# Database cache
include_once(DRUPAL_ROOT . '/includes/cache.inc');
include_once(DRUPAL_ROOT . '/sites/all/modules/domain_cache/domain_cache_database.inc');
$conf['cache_default_class'] = 'DrupalDomainDatabaseCache';

# Memcache
// Add this AFTER the memcache include.
include_once(DRUPAL_ROOT . '/includes/cache.inc');
include_once(DRUPAL_ROOT . '/sites/all/modules/domain_cache/domain_cache_memcache.inc');
$conf['cache_default_class'] = 'DrupalDomainMemCache';


Step 2.
-------------------------------------------------------------------------------- 
Optionally install the Domain Cache module to exclude bins from separating 
cache keys per domain. This module will not separate cache keys for the
cache_bootstrap table.
   
You may also exclude tables by setting the $conf['domain_cache_excluded_bins'] 
variable in settings.php

Example:

$conf['domain_cache_excluded_bins'] = array(
  'cache_bootstrap',
  'cache_menu',
);

This should be done before the includes in Step 1.